import React, { Component } from 'react';
import Header from './routing/Header.jsx'
import Main from './routing/Main.jsx'

class App extends Component{
  render(){
    return(
      <div>
        <Header />
        <Main />
      </div>
      );
  }
}

export default App;
