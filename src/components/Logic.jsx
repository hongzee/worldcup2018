import React, { Component } from 'react';
import '../css/App.css';
import '../css/groups.css';
import { Grid, Form, Button, ControlLabel } from 'react-bootstrap';
import { FormControl, Alert, Well, Col, Row } from 'react-bootstrap';
import { GameWall } from './GameWall.jsx'
import { UserData } from './UserData.jsx'

const url = require('../url.js').url

export class Logic extends Component {

  constructor(props){
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.state = {show:null}
  }

  handleSubmit(event){
    event.preventDefault();
    let len = event.target.elements.length - 2
    let password = event.target.elements[len].value
    let elements = event.target.elements

    fetch(url +"/check_password",
    {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify({pass:password})
    })
    .then((res)=>{
      return (res.json())
    })
    .then((data)=>{
      if (data.message){
          this.setState({show:false})
          let results = {info:{},games:{}}
          for (var i = 0; i < elements.length - 2; i++) {
            let info = elements[i].id.split('_')
            let id = info[0]
            if (!isNaN(id)){
              let place = info[1]
              let name = elements[i].name
              let value = elements[i].value
              if (!(id in results.games))
                results.games[id] = {[place]:{name:name,value:value}}
              else
                results.games[id][place] = {name:name,value:value}
            }
            else{
              results.info[id] = elements[i].value
            }
          }
          fetch(url + "/create",
          {
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
              method: "POST",
              body: JSON.stringify(results)
          })
          .then((res)=>(res.json()))
          .then((data)=>{console.log(data)})
          .catch((res)=>{ console.log(res) })
        }
      else this.setState({show:true})
    })
    .catch((res)=>{ console.log(res)})
  }




  render() {
    let error = <Alert bsStyle="warning" style={{display:'inline'}}>
                  <strong>Holy guacamole!</strong> Wrong password.
                  </Alert>
    let success = <Alert bsStyle="success" style={{display:'inline'}}>
                  <strong>Great!</strong> All ok.
                  </Alert>
    if (this.state.show === null) success = null



    return (
      <Grid fluid>
        <Form inline onSubmit={this.handleSubmit}>
          <UserData/>
          <GameWall/>
          <Well className='user-data'>
          <Row>
            <Col xs={4} sm={4} xsOffset={4}  md={4} lg={4}>
              <ControlLabel>Password:</ControlLabel><br/>
              <FormControl id='password' type='text'></FormControl>
              <Button type='submit'>Submit</Button>
              { this.state.show ? error : success }
              <div style={{marginBottom:'100px'}}></div>
            </Col>
          </Row>
          </Well>

        </Form>
      </Grid>
    );
  }
}
