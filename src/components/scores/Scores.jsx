import React, { Component } from 'react';
import {Col, Row} from 'react-bootstrap';
import {Grid, Well } from 'react-bootstrap';
import Chart from './ScoreChart.jsx';
import UserScore from './UserScore.jsx';
//import TableTitle from '../titles.TableTitle.jsx'

const url = require('../../url.js').url

class Scores extends Component{
  render(){
    return(
      <Grid>
        <Well className='title'>
          <h2>Tabla de Puntajes</h2>
        </Well>
        <Row>
          <Col xs={6}>
            <Chart url={url}/>
          </Col>
          <Col xs={6}>
            <Well>

            <UserScore url={url}/>
            </Well>
          </Col>
        </Row>
      </Grid>


    )
  }
}

export default Scores;
