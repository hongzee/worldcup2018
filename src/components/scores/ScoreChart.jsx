import React, { Component } from 'react';
import { Table } from 'react-bootstrap';


class Chart extends Component{
  constructor(props){
    super(props)
    this.state = {chart:[]}
  }
  componentWillMount(){
    fetch(this.props.url + "/scores")
    .then((res)=>{
      return (res.json())
    })
    .then((data)=>{
      data = UserSort(data).reverse()
      this.setState({chart:data})
    })
  }

  render(){
    let chart = this.state.chart
    let elements = []
    chart.forEach((user,index)=>{
      elements.push(
        <tr key={index + '.0'}>
          <td key={index + '.1'}>{index + 1}</td>
          <td key={index + '.2'}>{user.user}</td>
          <td key={index + '.3'}>{user.points}</td>
        </tr>
      )
    })

    return(
      <div className="panel-default panel">
      <Table striped bordered condensed hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Usuario</th>
            <th>Puntos</th>
          </tr>
        </thead>
        <tbody>
        {elements}
        </tbody>
      </Table>
      </div>
    )
  }
}

// swap function helper
function swap(array, i, j) {
  var temp = array[i];
  array[i] = array[j];
  array[j] = temp;
}

function UserSort(array){
  for (var i = 0; i < array.length; i++) {
    for (var j = 1; j < array.length; j++) {
      if(array[j - 1].points > array[j].points) {
        swap(array, j - 1, j);
      }
    }
  }
  return array
}

export default Chart;
