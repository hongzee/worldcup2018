import React, { Component } from 'react';
import {Col, Row} from 'react-bootstrap';
import {Form, ControlLabel, FormControl, Button} from 'react-bootstrap';

class UserScore extends Component{
  constructor(props){
    super(props)
    this.state = {form:'',score:''}
    this.submitHandler = this.submitHandler.bind(this)
    this.changeHandler = this.changeHandler.bind(this)
  }

  changeHandler(event){
    this.setState({form:event.target.value})
  }

  submitHandler(event){

    event.preventDefault()
    fetch(this.props.url + "/scores/" + this.state.form)
    .then((res)=>{
      return (res.json())
    })
    .then((data)=>{
      if (data)
      this.setState({score:data.points})
    })
  }

  render(){
    return(
      <Form onSubmit={this.submitHandler}>
        <ControlLabel>Buscar Usuario:</ControlLabel><br/>
        <Row>
        <Col xs={6}>
        <FormControl type='text' onChange={this.changeHandler} style={{width:'100px'}}></FormControl>
        <Button type='submit' style={{width:'100px'}}>Buscar</Button>
        </Col>
        <Col xs={6}>
        <h2 style={{textAlign:'center'}}>{this.state.score}</h2>
        </Col>
          
        </Row>
      </Form>
    )
  }
}

export default UserScore;