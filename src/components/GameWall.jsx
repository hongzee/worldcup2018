import React, { Component } from 'react';
import { GroupStage } from './groups/GroupStage.jsx'
import { PlayOff } from './playoff/PlayOff.jsx'

export class GameWall extends Component{
  constructor(props){
    super(props)
    this.state = {
      'Rusia':{points:0,gf:0,gc:0},'Arabia Saudita':{points:0,gf:0,gc:0},
      'Egipto':{points:0,gf:0,gc:0}, 'Uruguay':{points:0,gf:0,gc:0},
      'Portugal':{points:0,gf:0,gc:0},'Marruecos':{points:0,gf:0,gc:0},
      'España':{points:0,gf:0,gc:0}, 'Iran':{points:0,gf:0,gc:0},
      'Francia':{points:0,gf:0,gc:0},'Dinamarca':{points:0,gf:0,gc:0},
      'Australia':{points:0,gf:0,gc:0}, 'Peru':{points:0,gf:0,gc:0},
      'Islandia':{points:0,gf:0,gc:0},'Nigeria':{points:0,gf:0,gc:0},
      'Croacia':{points:0,gf:0,gc:0}, 'Argentina':{points:0,gf:0,gc:0},
      'Brasil':{points:0,gf:0,gc:0},'Costa Rica':{points:0,gf:0,gc:0},
      'Serbia':{points:0,gf:0,gc:0}, 'Suiza':{points:0,gf:0,gc:0},
      'Suecia':{points:0,gf:0,gc:0},'Corea del Sur':{points:0,gf:0,gc:0},
      'Mexico':{points:0,gf:0,gc:0}, 'Alemania':{points:0,gf:0,gc:0},
      'Belgica':{points:0,gf:0,gc:0},'Tunez':{points:0,gf:0,gc:0},
      'Inglaterra':{points:0,gf:0,gc:0}, 'Panama':{points:0,gf:0,gc:0},
      'Polonia':{points:0,gf:0,gc:0},'Senegal':{points:0,gf:0,gc:0},
      'Japon':{points:0,gf:0,gc:0}, 'Colombia':{points:0,gf:0,gc:0}
    }

    this.groups = {
    'A': ['Rusia','Arabia Saudita', 'Uruguay', 'Egipto'],
    'B': ['Portugal','Marruecos', 'España', 'Iran'],
    'C': ['Francia','Dinamarca', 'Australia', 'Peru'],
    'D': ['Islandia','Nigeria', 'Croacia', 'Argentina'],
    'E': ['Brasil','Costa Rica', 'Serbia', 'Suiza'],
    'F': ['Suecia','Corea del Sur', 'Mexico', 'Alemania'],
    'G': ['Belgica','Tunez', 'Inglaterra', 'Panama'],
    'H': ['Polonia','Senegal', 'Japon', 'Colombia']}
    this.results = {}
    this.groupResults = {}
    this.onChangeHandler = this.onChangeHandler.bind(this)
    this.playOffHandler = this.playOffHandler.bind(this)
  }
  onChangeHandler(results){
    let id = results['id']
    let keys = Object.keys(results['countries'])
    let result_1 = results['countries'][keys[0]]
    let result_2 = results['countries'][keys[1]]
    let winner
    let loser
    if (!this.results[id]){
      this.results[id] = results['countries']
      if (result_1 > result_2){
        winner = keys[0]
        loser = keys[1]
      }
      else if (result_1 < result_2) {
        winner = keys[1]
        loser = keys[0]
      }
      else {winner = null}
      if (winner){
        this.setState((prevState,props) => {
          let property_win = prevState[winner]
          property_win.points += 3
          property_win.gf += results['countries'][winner]
          property_win.gc += results['countries'][loser]
          let property_lose = prevState[loser]
          property_lose.gf += results['countries'][loser]
          property_lose.gc += results['countries'][winner]
          return ({[winner]:property_win,[loser]:property_lose})
        })
      }
      else {
        this.setState((prevState,props) => {
          let goals = results['countries'][keys[0]]
          let property_win = prevState[keys[0]]
          property_win.points += 1
          property_win.gf += goals
          property_win.gc += goals
          let property_lose = prevState[keys[1]]
          property_lose.points += 1
          property_lose.gf += goals
          property_lose.gc += goals
          return ({[keys[0]]:property_win,[keys[1]]:property_lose})
        })

      }
    }
    else {

      let last_result_1 = this.results[id][keys[0]]
      let last_result_2 = this.results[id][keys[1]]
      let last_result = this.results[id]

      let diff_1 = last_result_1 - last_result_2
      let diff_2 = result_1 - result_2
      this.results[id] = results['countries']
      //if (!((diff_1>0) && (diff_2>0)) || !((diff_1<0) && (diff_2<0))  || !((diff_1===0) && (diff_2===0))){

        if (diff_1 === 0 && diff_2 > 0){
          this.setState((prevState,props) =>{
            let property_1 = prevState[keys[0]]
            let property_2 = prevState[keys[1]]
            property_1.points += - 1 + 3
            property_2.points += - 1
            property_1.gf += results['countries'][keys[0]] - last_result[keys[0]]
            property_2.gf += results['countries'][keys[1]] - last_result[keys[1]]
            property_1.gc += results['countries'][keys[1]] - last_result[keys[1]]
            property_2.gc += results['countries'][keys[0]] - last_result[keys[0]]

            return({
            [keys[0]]: property_1,
            [keys[1]]: property_2})
          })
        }
        else if (diff_1 === 0 && diff_2 < 0){
          this.setState((prevState,props) =>{
            let property_1 = prevState[keys[0]]
            let property_2 = prevState[keys[1]]
            property_1.points -= 1
            property_2.points += 3 - 1
            property_1.gf += results['countries'][keys[0]] - last_result[keys[0]]
            property_2.gf += results['countries'][keys[1]] - last_result[keys[1]]
            property_1.gc += results['countries'][keys[1]] - last_result[keys[1]]
            property_2.gc += results['countries'][keys[0]] - last_result[keys[0]]

            return({
            [keys[0]]: property_1,
            [keys[1]]: property_2})
          })
        }
        else if (diff_1 > 0 && diff_2 < 0){
          this.setState((prevState,props) =>{
            let property_1 = prevState[keys[0]]
            let property_2 = prevState[keys[1]]
            property_1.points -= 3
            property_2.points += 3
            property_1.gf += results['countries'][keys[0]] - last_result[keys[0]]
            property_2.gf += results['countries'][keys[1]] - last_result[keys[1]]
            property_1.gc += results['countries'][keys[1]] - last_result[keys[1]]
            property_2.gc += results['countries'][keys[0]] - last_result[keys[0]]
            return({
            [keys[0]]: property_1,
            [keys[1]]: property_2})
          })
        }
        else if (diff_1 > 0 && diff_2 === 0){
          this.setState((prevState,props) =>{
            let property_1 = prevState[keys[0]]
            let property_2 = prevState[keys[1]]
            property_1.points += 1 - 3
            property_2.points += 1
            property_1.gf += results['countries'][keys[0]] - last_result[keys[0]]
            property_2.gf += results['countries'][keys[1]] - last_result[keys[1]]
            property_1.gc += results['countries'][keys[1]] - last_result[keys[1]]
            property_2.gc += results['countries'][keys[0]] - last_result[keys[0]]

            return({
            [keys[0]]: property_1,
            [keys[1]]: property_2})
          })
        }
        else if (diff_1 < 0 && diff_2 > 0){
          this.setState((prevState,props) =>{
            let property_1 = prevState[keys[0]]
            let property_2 = prevState[keys[1]]
            property_1.points += 3
            property_2.points -= 3
            property_1.gf += results['countries'][keys[0]] - last_result[keys[0]]
            property_2.gf += results['countries'][keys[1]] - last_result[keys[1]]
            property_1.gc += results['countries'][keys[1]] - last_result[keys[1]]
            property_2.gc += results['countries'][keys[0]] - last_result[keys[0]]

            return({
            [keys[0]]: property_1,
            [keys[1]]: property_2})
          })
        }
        else if (diff_1 < 0 && diff_2 === 0){
          this.setState((prevState,props) =>{
            let property_1 = prevState[keys[0]]
            let property_2 = prevState[keys[1]]
            property_1.points += 1
            property_2.points += 1 - 3
            property_1.gf += results['countries'][keys[0]] - last_result[keys[0]]
            property_2.gf += results['countries'][keys[1]] - last_result[keys[1]]
            property_1.gc += results['countries'][keys[1]] - last_result[keys[1]]
            property_2.gc += results['countries'][keys[0]] - last_result[keys[0]]
            return({
            [keys[0]]: property_1,
            [keys[1]]: property_2})
          })
        }

        else {
          this.setState((prevState,props) =>{
            let property_1 = prevState[keys[0]]
            let property_2 = prevState[keys[1]]
            property_1.gf += results['countries'][keys[0]] - last_result[keys[0]]
            property_2.gf += results['countries'][keys[1]] - last_result[keys[1]]
            property_1.gc += results['countries'][keys[1]] - last_result[keys[1]]
            property_2.gc += results['countries'][keys[0]] - last_result[keys[0]]
            return({
            [keys[0]]: property_1,
            [keys[1]]: property_2})
          })
        }

    }

  }

  playOffHandler(group,first,second){
    this.groupResults[group] = [first,second]
  }

  render(){
    return(
      <div>
      <GroupStage onChangeHandler={this.onChangeHandler} groups={this.groups} points={this.state} playOffHandler={this.playOffHandler} />
      <PlayOff groupResults={this.groupResults}/>
      </div>
      );
  }
}
