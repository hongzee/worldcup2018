import React, { Component } from 'react';
import { FormGroup, FormControl, ControlLabel } from 'react-bootstrap';
import { DropdownButton, MenuItem } from 'react-bootstrap';

export class PlayOffGame extends Component{

  constructor(props){
    super(props)
    this.state = {title1:'',title2:'',array1:null,array2:null}
  }

  componentWillMount(){
    let pais1 = this.props.pais1 ? this.props.pais1 : 'To be Determined'
    let pais2 = this.props.pais2 ? this.props.pais2 : 'To be Determined'
    this.setState({'title2':pais2})
    this.setState({'title1':pais1})
  }

  componentWillReceiveProps(nextProps){
    let pais1 = nextProps.pais1
    let pais2 = nextProps.pais2
    if (pais1){
      if (!(pais1.constructor === Array))
        this.setState({title1:pais1})
      else if (this.state.array1 === null) this.setState({array1:true})
    }
    if (pais2){
      if (!(pais2.constructor === Array))
        this.setState({title2:pais2})
      else if (this.state.array2 === null) this.setState({array2:true})
    }
    let old_pais1 = this.props.pais1
    let old_pais2 = this.props.pais2
    if (this.props.id < 57){
      if ((pais1 !== old_pais1) && old_pais1)
      this.props.fixHandler(pais1,old_pais1)
      if ((pais2 !== old_pais2) && old_pais2)
      this.props.fixHandler(pais2,old_pais2)
    }
  }

  render(){

    // Pais 1
    let pais1 = this.props.pais1 ? this.props.pais1 : 'To be Determined'
    let pais2 = this.props.pais2 ? this.props.pais2 : 'To be Determined'
    let label1
    let label2
    if (pais1.constructor === Array) {
      let elements = []
      for (var i = 0; i < pais1.length; i++) {
        elements.push(<MenuItem name={i?pais1[0]:pais1[1]} key={i} eventKey={i}>{pais1[i]}</MenuItem>)
      }
      let title1 = this.state['title1']
      if (title1 === 'To be Determined')  title1 = ''
      label1 = [
      <DropdownButton id={0} key={i+1} title={title1} style={{width:'100px'}} onSelect={(eventKey, event)=>{
        this.setState({'title1':event.target.text,'array1':false})
        if (this.props.id === '64')
        this.props.thirdHandler(0,event.target.name)
      }}>
      {elements}
      </DropdownButton>]
    }
    else{
      label1 = <ControlLabel style={{width:'100px'}}>{pais1}</ControlLabel>
    }



    // Pais 2
    if (pais2.constructor === Array){
      let elements = []

      for (i = 0; i < pais2.length; i++) {
        elements.push(<MenuItem name={i?pais2[0]:pais2[1]} key={i} eventKey={i}>{pais2[i]}</MenuItem>)
      }
      let title2 = this.state['title2']
      if (title2 === 'To be Determined')  title2 = ''
      label2 =
      [<DropdownButton id={0} key={i+1} title={title2} style={{width:'100px'}} onSelect={(eventKey, event)=>{
        this.setState({'title2':event.target.text,'array2':false})
        if (this.props.id === '64'){
        this.props.thirdHandler(1,event.target.name)
        }
      }}>
      {elements}
      </DropdownButton>]
    }
    else{
      label2 = <ControlLabel style={{width:'100px'}}>{pais2}</ControlLabel>

    }
    // Rendering
    return(
      <FormGroup className='game' onChange={(event)=>{
        if (event.target.value){
          if (!isNaN(event.target.value)){
            let number = parseInt(event.target.value,10)
            if (number >= 0){
              this.setState({[event.target.name]:number},()=>{
                let winner
                let loser
                let pais1 = this.state['title1']
                let pais2 = this.state['title2']
                if (this.state[pais1] > this.state[pais2]){
                  winner = pais1
                  loser = pais2
                }
                else if (this.state[pais1] < this.state[pais2]){
                  winner = pais2
                  loser = pais1
                }
                else if (this.state[pais1] === this.state[pais2]){
                  winner = [pais1,pais2]
                }
                let place = this.props.place
                let next = this.props.next
                this.props.matchHandler(next,place,winner,loser)
              })
            }
          }
        }
      }}>
        {label1}
        <FormControl
          type='text'
          pattern="[0-9]+"
          style={{width:'40px', display:'inline-block'}}
          name={this.state['title1']}
          disabled={(this.props.pais1&&(!this.state.array1))?false:true}
          id = {this.props.id.toString() + '_0'}
          required
          />


        <FormControl
          type='text'
          pattern="[0-9]+"
          style={{width:'40px', display:'inline-block'}}
          name={this.state['title2']}
          disabled={(this.props.pais2&&(!this.state.array2))?false:true}
          id = {this.props.id.toString() + '_1'}
          required
          />
        {label2}
      </FormGroup>
    );
  }
}
