import React, { Component } from 'react';
import { Panel, Row, Col } from 'react-bootstrap';

export class PlayOffStage extends Component{
  render(){
    return(
    <Row>
      <Col className='subgroup' xs={12} sm={12} md={12} lg={12}>
        <Panel className={'panel-group stage-' + this.props.cname}>
          <Panel.Heading className={'panel-title stage-' + this.props.cname}>
            <Panel.Title >
              {this.props.title}
            </Panel.Title>
          </Panel.Heading>
            <Panel.Body style={this.props.cname==='3'?{paddingBottom:'68px'}:{}}>
                {this.props.games}
            </Panel.Body>
        </Panel>
      </Col>
    </Row>
    )
  }
}

