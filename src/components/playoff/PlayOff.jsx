import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import { PlayOffGame } from './PlayOffGame.jsx';
import { PlayOffStage} from './PlayOffStage.jsx'


export class PlayOff extends Component{

  constructor(props){
    super(props)
    this.state = {
      57:[null,null],
      58:[null,null], 59:[null,null], 60:[null,null],
      61:[null,null], 62:[null,null], 63:[null,null],
      64:[null,null]
    }
    this.nextGame = {
      49:57,50:59,51:57,52:59,
      53:58,54:60,55:58,56:60,
      57:61,58:61,59:62,60:62,
      61:64,62:64,64:65,63:65
    }
    this.matchHandler = this.matchHandler.bind(this)
    this.thirdplaceHandler = this.thirdplaceHandler.bind(this)
    this.fixHandler = this.fixHandler.bind(this)
  }

  matchHandler(next,place,winner,loser){
    if (next === 64){
      this.setState((prevState,props)=>{
        let list = prevState[63]
        list[place] = loser
        return {63:list}
      })
    }

    if (next !== 65){
      this.setState((prevState,props)=>{
        let list = prevState[next]
        list[place] = winner
        return {[next]:list}
      },()=>{
        if (loser){
          if (this.state[this.nextGame[next]]){
            if (loser === this.state[this.nextGame[next]][0]){
              if (this.nextGame[next] === 64){loser = this.state[next][place?0:1]}
              this.matchHandler(this.nextGame[next],0,winner,loser)
            }
            else if (loser === this.state[this.nextGame[next]][1]){
              if (this.nextGame[next] === 64){loser = this.state[next][place?0:1]}
              this.matchHandler(this.nextGame[next],1,winner,loser)
            }
          }
        }
      })
    }
  }

  fixHandler(new_item,old_item){
    for (let key in this.state){
      let list = this.state[key]
      if (list[0] === old_item){
        list = [new_item,list[1]]
        this.setState({[key]:list})
      }
      else if (list[1] === old_item){
        list = [list[0],new_item]
        this.setState({[key]:list})
      }
    }
  }

  thirdplaceHandler(place, country){
    this.setState((prevState,props)=>{
      let list = prevState[63]
      list[place] = country
      return {63:list}
    })
  }

  createFinal16(){
    let groups = this.props.groupResults
    let elements = [
    <div key={0}>
    <Col xs={12} sm={12} md={6} lg={6}>
    <PlayOffGame key={49} id={49} next={57} place={0} pais1={groups['A'][0]} pais2={groups['B'][1]} matchHandler={this.matchHandler} fixHandler={this.fixHandler} />
    </Col>
    <Col xs={12} sm={12} md={6} lg={6}>
    <PlayOffGame key={51} id={50} next={59} place={0} pais1={groups['A'][1]} pais2={groups['B'][0]} matchHandler={this.matchHandler} fixHandler={this.fixHandler}/>
    </Col>
    <Col xs={12} sm={12} md={6} lg={6}>
    <PlayOffGame key={50} id={51} next={57} place={1} pais1={groups['C'][0]} pais2={groups['D'][1]} matchHandler={this.matchHandler} fixHandler={this.fixHandler}/>
    </Col>
    <Col xs={12} sm={12} md={6} lg={6}>
    <PlayOffGame key={52} id={52} next={59} place={1} pais1={groups['C'][1]} pais2={groups['D'][0]} matchHandler={this.matchHandler} fixHandler={this.fixHandler}/>
    </Col>
    <Col xs={12} sm={12} md={6} lg={6}>
    <PlayOffGame key={53} id={53} next={58} place={0} pais1={groups['E'][0]} pais2={groups['F'][1]} matchHandler={this.matchHandler} fixHandler={this.fixHandler}/>
    </Col>
    <Col xs={12} sm={12} md={6} lg={6}>
    <PlayOffGame key={55} id={54} next={60} place={0} pais1={groups['E'][1]} pais2={groups['F'][0]} matchHandler={this.matchHandler} fixHandler={this.fixHandler}/>
    </Col>
    <Col xs={12} sm={12} md={6} lg={6}>
    <PlayOffGame key={54} id={55} next={58} place={1} pais1={groups['G'][0]} pais2={groups['H'][1]} matchHandler={this.matchHandler} fixHandler={this.fixHandler}/>
    </Col>
    <Col xs={12} sm={12} md={6} lg={6}>
    <PlayOffGame key={56} id={56} next={60} place={1} pais1={groups['G'][1]} pais2={groups['H'][0]} matchHandler={this.matchHandler} fixHandler={this.fixHandler}/>
    </Col>
    </div>]
    return elements
  }

  createFinal8(){
    let cuartos = []
    let semi = []
    let final = []
    let tercer = []
    for (let key in this.state){
      let game = this.state[key]
      let width = key <= 60 ? 6 : 12
      if (game){

        let element = (
          <Col key={key} xs={12} sm={12} md={width} lg={width}>
          <PlayOffGame
            key={key}
            id={key}
            next={this.nextGame[key]}
            place={(key%2)?0:1}
            pais1={game[0]}
            pais2={game[1]}
            matchHandler={this.matchHandler}
            thirdHandler={this.thirdplaceHandler}
            fixHandler={this.fixHandler}/>
          </Col>)
        if (key <= 60) cuartos.push(element)
        else if (key <= 62) semi.push(element)
        else if (key === '63') tercer.push(element)
        else if (key === '64') final.push(element)
      }
    }
    return {cuartos:cuartos,semi:semi,tercer:tercer,final:final}
  }



  render(){

    let final8 = this.createFinal8()
    let final16= this.createFinal16()
    return(
      <div>
        <Row>
          <Col xs={12} sm={12} md={12} lg={12}>
          <PlayOffStage title='8vos de Final' games={final16} cname='8'/>
          </Col>
        </Row>
        <Row>
          <Col xs={12} sm={12} md={12} lg={12}>
          <PlayOffStage title='4tos de Final' games={final8.cuartos} cname='4' />
          </Col>
        </Row>
        <Row>
          <Col xs={12} sm={12} md={6} lg={6}>
            <PlayOffStage title='Semi Final' games={final8.semi} cname='2' />
          </Col>
          <Col xs={12} sm={12} md={6} lg={6} >
            <PlayOffStage title='3er Lugar' games={final8.tercer} cname='3'/>
          </Col>
        </Row>
        <Row>
          <Col xs={12} sm={12} md={12} lg={12}>
            <PlayOffStage title='Final' games={final8.final} cname='1' />
          </Col>
        </Row>

      </div>

      );
  }
}
