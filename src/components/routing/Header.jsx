import React, { Component } from 'react';
import { LinkContainer } from 'react-router-bootstrap';
import {Navbar, Nav, NavItem} from 'react-bootstrap';
// The Header creates links that can be used to navigate
// between routes.
class Header extends Component{
  render(){
    return(
        <header>
        <Navbar inverse fixedTop>
          <Navbar.Header>
            <Navbar.Brand>
              Polla Mundialera
            </Navbar.Brand>
          </Navbar.Header>
          <Nav pullRight>
            <LinkContainer exact to='/home' activeClassName="active-link">
              <NavItem>
              Inicio
              </NavItem>
            </LinkContainer>
            <LinkContainer exact to='/game' activeClassName="active-link">
              <NavItem>
              Crear Polla
              </NavItem>
            </LinkContainer>
            <LinkContainer exact to='/scores' activeClassName="active-link">
              <NavItem>
              Tabla Puntajes
              </NavItem>
            </LinkContainer>
          </Nav>
        </Navbar>
      </header>
    );
  }
}

export default Header;
