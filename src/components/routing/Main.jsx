import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom'
import {GameWindow } from '../GameWindow.jsx'
import Home from '../Home.jsx'
import Admin from '../Admin.jsx'
import Scores from '../scores/Scores.jsx'

class Main extends Component{
  render(){
    return(
      <main>
        <Switch>
          <Route exact path='/home' component={Home}/>
          <Route exact path='/' component={GameWindow}/>
          <Route exact path='/game' component={GameWindow}/>
          <Route exact path='/admin' component={Admin}/>
          <Route exact path='/scores' component={Scores}/>
        </Switch>
      </main>
    );
  }
}

export default  Main;
