import React, { Component } from 'react';
import { Logic } from './Logic.jsx'
import { GameTitle } from './titles/GameTitle.jsx'

export class GameWindow extends Component{
  render(){
    return(
      <div>
        <GameTitle/>
        <Logic/>
      </div>

    );
  }
}
