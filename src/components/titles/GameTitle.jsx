import React, { Component } from 'react';
import {Well, Grid} from 'react-bootstrap';

export class GameTitle extends Component{
  render(){
    return(
      <Grid fluid>
        <Well className='title'>
          <h2>Crear Polla</h2>
          <p>En esta seccion podras llenar tu prónostico para cada partido del mundial.
          El pronóstico debe ser coherente, por lo que los primeros de cada grupo
          pasarán automaticamente a la ronda eliminatoria. Primero ingresa tus datos para
          poder registrarte, y comienza a llenar la planilla!!</p>
        </Well>
      </Grid>

    );
  }
}
