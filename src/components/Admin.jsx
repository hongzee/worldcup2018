import React, { Component } from 'react';
import { Grid, Row, Col, Button, Alert} from 'react-bootstrap'
import {Form, ControlLabel, FormControl} from 'react-bootstrap'

const url = require('../url.js').url

const countries = [
      'Rusia','Arabia Saudita','Egipto', 'Uruguay',
      'Portugal','Marruecos', 'España', 'Iran',
      'Francia','Dinamarca', 'Australia', 'Peru',
      'Islandia','Nigeria', 'Croacia', 'Argentina',
      'Brasil','Costa Rica', 'Serbia', 'Suiza',
      'Suecia','Corea del Sur', 'Mexico', 'Alemania',
      'Belgica','Tunez', 'Inglaterra', 'Panama',
      'Polonia','Senegal', 'Japon', 'Colombia'
    ]

class Admin extends Component{
  constructor(props){
    super(props)
    this.state = {admin_pass:'',message:'', pais1:'Rusia', pais2:'Rusia'}
    this.handleSubmit = this.handleSubmit.bind(this)
    this.changeHandler = this.changeHandler.bind(this)
    this.updateHandler = this.updateHandler.bind(this)
    this.sendResultHandler = this.sendResultHandler.bind(this)

  }

  handleSubmit(event){
    event.preventDefault()
    fetch(url + "/get_password",
    {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify({admin_pass:this.state.admin_pass})
    })
    .then((res)=>{
      return (res.json())
    })
    .then((data)=>{
      this.setState({message:data.pass})
    })
  }

  changeHandler(event){
    this.setState({[event.target.name]:event.target.value})
  }

  updateHandler(){
    fetch(url + "/update_request",
    {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify({admin_pass:this.state.admin_pass})
    })
    .then((res)=>{
      return (res.json())
    })
    .then((data)=>{
      if (data.message){
        this.setState({message:'Updating...'})
        fetch(url + "/update",
        {
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify({})
        })
        .then((res)=>(res.json()))
        .then((data)=>{console.log(data.message)})
      }
      else console.log('Incorrect Password')
    })
  }

  sendResultHandler(){
    fetch(url + "/update_request",
    {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify({admin_pass:this.state.admin_pass})
    })
    .then((res)=>{
      return (res.json())
    })
    .then((data)=>{
      if (data.message){
        this.setState({message:'Result Sent'})
        let result = {
          id:this.state.id,
          pais1:this.state.pais1,
          result1:this.state.result1,
          pais2:this.state.pais2,
          result2:this.state.result2
        }
        fetch(url + "/send_result",
        {
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify({result})
        })
        .then((res)=>(res.json()))
        .then((data)=>{console.log(data.message)})
      }
      else console.log('Incorrect Password')
    })
  }

  render(){
    let success = <Alert bsStyle="success" style={{marginTop:'20px', width:'200px'}}>
                  <strong>{this.state.message}</strong>
                  </Alert>
    let elements = []
    countries.forEach((country, index)=>{
      elements.push(<option key={index} value={country}>{country}</option>)
    })

    return(
      <Grid>
        <Row>
          <Form onSubmit={this.handleSubmit} style={{marginTop:'50px'}}>
            <Col sm={5} smOffset={1} >
              <ControlLabel>Password</ControlLabel>
              <FormControl name='admin_pass' onChange={this.changeHandler} id='password' type='password' style={{width:'200px'}}></FormControl>
              <Button onClick={this.sendResultHandler} style={{width:'200px'}} >Send Result</Button>
              <Button onClick={this.updateHandler} style={{width:'200px'}} >Update Game</Button>
              <Button type='submit' style={{width:'200px'}} >Get Password</Button>
              {this.state.message ? success : null}
            </Col>
            <Col sm={5}>
              <ControlLabel>Id:</ControlLabel>
              <FormControl name='id' onChange={this.changeHandler} id='id' type='text' bsSize='sm' style={{width:'40px', display:'inline', margin:'0px 0px 0px 5px'}}></FormControl><br/>
              <ControlLabel>Pais 1:</ControlLabel>
              <FormControl name='pais1' onChange={this.changeHandler} componentClass="select" placeholder="select" style={{width:'150px', display:'inline', margin:'0px 5px 0px 5px'}}>
              {elements}
              </FormControl>
              <ControlLabel style={{display:'inline'}}>:</ControlLabel>
              <FormControl name='result1' onChange={this.changeHandler} id='result1' type='text' bsSize='sm' style={{width:'40px', display:'inline', margin:'0px 0px 0px 5px'}}></FormControl><br/>
              <ControlLabel>Pais 2:</ControlLabel>
              <FormControl name='pais2' onChange={this.changeHandler} componentClass="select" placeholder="select" style={{width:'150px', display:'inline', margin:'0px 5px 0px 5px'}}>
              {elements}
              </FormControl>
              <ControlLabel style={{display:'inline'}}>:</ControlLabel>
              <FormControl name='result2' onChange={this.changeHandler} id='result2' type='text' bsSize='sm' style={{width:'40px', display:'inline', margin:'0px 0px 0px 5px'}}></FormControl>
            </Col>
          </Form>
        </Row>
      </Grid>

    );
  }
}

export default Admin;
