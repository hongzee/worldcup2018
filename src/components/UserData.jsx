import React, { Component } from 'react';
import {Row, Col, Well, FormGroup, FormControl, ControlLabel} from 'react-bootstrap'

export class UserData extends Component{
  render(){
    return(
      <Well className='user-data'>
      <Row>
        <Col xs={4} sm={4} md={4} lg={4}>
        <h2>Ingresa tus Datos</h2>
        </Col>

        <Col xs={4} sm={4} md={4} lg={4}>
          <FormGroup >
            <ControlLabel>Nombre</ControlLabel><br/>
            <FormControl id='name' type='text' required></FormControl><br/>
            <ControlLabel>Email</ControlLabel><br/>
            <FormControl id='email' type='email' required></FormControl>
          </FormGroup>
        </Col>
        <Col xs={4} sm={4} md={4} lg={4}>
          <FormGroup >
            <ControlLabel>Usuario</ControlLabel><br/>
            <FormControl  id='user' type='text' required></FormControl><br/>
            <ControlLabel>Telefono</ControlLabel><br/>
            <FormControl id='phone' type='text' required></FormControl><br/>
          </FormGroup>
        </Col>
      </Row>
      </Well>

    );
  }
}
