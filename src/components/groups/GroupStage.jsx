import React, { Component } from 'react';
import {Row, Col, Panel} from 'react-bootstrap';
import {Group} from './Group.jsx'
import {GamesFixture} from './GamesFixture.jsx'

const fixture = require('../../fixture.json')

export class GroupStage extends Component{

  constructor(props){
    super(props)
    this.groups = this.props.groups
    this.state = {0:true,2:false,4:false,6:false}
    this.handleToggle = this.handleToggle.bind(this)
  }

  handleToggle(i){
    this.setState((prevState)=>({[i]:!prevState[i]}))
  }


  render(){
    let groups = ['A','B','C','D','E','F','G','H']
    let elements = []
    for (let i = 0; i < groups.length; i=i+2) {
      let element1 = (
        <Col key={i + '.1'} xs={12} sm={12} md={6} lg={6}>
        <Panel className={'panel-group group-' + groups[i]} onToggle={()=>{}} expanded={this.state[i]}>
          <Panel.Heading className={'panel-title group-' + groups[i]} onClick={()=>{this.handleToggle(i)}}>
            <Panel.Title toggle>
              Grupo {groups[i]}
            </Panel.Title>
          </Panel.Heading>
          <Panel.Collapse>
            <Panel.Body>
              <GamesFixture
                key={i + '.2'}
                group={groups[i]}
                countries={fixture[groups[i]]}
                onChangeHandler={this.props.onChangeHandler}/>
              <Group
                key={i + '.3'}
                points={this.props.points}
                group={groups[i]}
                countries={this.groups}
                playOffHandler={this.props.playOffHandler}/>
            </Panel.Body>
          </Panel.Collapse>
        </Panel>
        </Col>
        )
      let element2 = (
        <Col key={(i+1) + '.1'} xs={12} sm={12} md={6} lg={6}>
        <Panel className={'panel-group group-' + groups[i+1]} onToggle={()=>{}} expanded={this.state[i]}>
          <Panel.Heading className={'panel-title group-' + groups[i+1]} onClick={()=>{this.handleToggle(i)}}>
            <Panel.Title toggle>
              Grupo {groups[i+1]}
            </Panel.Title>
          </Panel.Heading>
          <Panel.Collapse>
            <Panel.Body>
              <GamesFixture
                key={(i+1) + '.2'}
                group={groups[i+1]}
                countries={fixture[groups[i+1]]}
                onChangeHandler={this.props.onChangeHandler}/>
              <Group
                key={(i+1) + '.3'}
                points={this.props.points}
                group={groups[i+1]}
                countries={this.groups}
                playOffHandler={this.props.playOffHandler}/>
            </Panel.Body>
          </Panel.Collapse>
        </Panel>
        </Col>
        )
      elements.push(element1)
      elements.push(element2)
    }

    groups.forEach((group,index)=>{

    })
    return(
        <div>
        <Row className='show-grid'>
          {elements[0]}
          {elements[1]}

        </Row>
        <Row className='show-grid'>
          {elements[2]}
          {elements[3]}

        </Row>
        <Row className='show-grid'>
          {elements[4]}
          {elements[5]}

        </Row>
        <Row className='show-grid'>
          {elements[6]}
          {elements[7]}

        </Row>
        </div>



      );
  }
}

export default GroupStage;
