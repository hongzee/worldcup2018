import React, { Component } from 'react';
import {Col, Row} from 'react-bootstrap';
import {GameResult} from './GameResult.jsx'

// Todos los partidos de un grupo
export class GamesFixture extends Component{
  render() {
    let countries = this.props.countries
    let games = []
    let counter = 0
    countries.forEach((game)=>{
      let element = <GameResult key={counter + '.0'} id={game['id']} pais1={game['countries'][0]} pais2={game['countries'][1]} onChangeHandler={this.props.onChangeHandler}/>
      //let offset = counter % 2 ? 0:1
      games.push(

        <Col key={counter + '.1'} xs={12} sm={12} md={12} lg={12}>
        {element}
        </Col>
        )
      counter++
    })
    return (

        <div className='subgroup'>
          <Row>
          {games}
          </Row>
        </div>
      )
  }
}
