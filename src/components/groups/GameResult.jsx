import React, { Component } from 'react';
import {  FormGroup, FormControl, ControlLabel } from 'react-bootstrap';

const emoji = require('../../emojis.json')

export class GameResult extends Component {

  constructor(props){
    super(props)
    let pais1 = this.props.pais1
    let pais2 = this.props.pais2
    this.state = {[pais1]:null, [pais2]:null}
    this.countries = [this.props.pais1,this.props.pais2]
  }

  render(){
    let id = this.props.id.toString()
    return(
      <FormGroup name='game' className='game' onChange={(event)=>{

        if (event.target.value){
          if (!isNaN(event.target.value)){
            let number = parseInt(event.target.value,10)
            if (number >= 0){
              this.setState({[event.target.name]:number},()=>{
                if (!(null === Object.values(this.state)[0] || null === Object.values(this.state)[1])){
                  let result = {'id':this.props.id,'countries':this.state}
                  this.props.onChangeHandler(result)
                }
              })
            }
          }
        }
      }}>
        <ControlLabel style={{width:'150px'}}>{emoji[this.props.pais1] + id + ' ' + this.props.pais1}</ControlLabel>
        <FormControl
          type='text'
          pattern="[0-9]+"
          style={{width:'40px',display: 'inline-block'}}
          name={this.props.pais1}
          id = {id + '_0'}
          required
          />
        <FormControl
          type='text'
          pattern="[0-9]+"
          style={{width:'40px',display: 'inline-block'}}
          name={this.props.pais2}
          id = {id + '_1'}
          required
          />
        <ControlLabel style={{width:'150px'}}>{this.props.pais2 + ' ' + emoji[this.props.pais2]}</ControlLabel>
      </FormGroup>
    );
  }
}
