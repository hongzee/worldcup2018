import React, { Component } from 'react';
import {Col, Row} from 'react-bootstrap';
import { Table } from 'react-bootstrap';

// Resultados de un grupo
export class Group extends Component{

  render(){
    let list = this.props.countries[this.props.group]
    let points = {}
    list.forEach((key) => {
      points[key] = this.props.points[key]
    })
    let keysSorted = countrySort(points).reverse()
    let elements = []
    let counter = 1
    this.props.playOffHandler(this.props.group, keysSorted[0],keysSorted[1])
    keysSorted.forEach((key) =>{
      let element = 
      <tr key={counter}>
        <td>{counter++}</td>
        <td>{key}</td> 
        <td>{points[key].points}</td>
        <td>{points[key].gf - points[key].gc}</td>
        <td>{points[key].gf}</td>
        <td>{points[key].gc}</td>
      </tr>
      elements.push(element)
    })

    return(
          <Row>
            <Col className='panel-big' xs={12} sm={10} smOffset={1} mdOffset={1} md={10} lgOffset={2} lg={8}>
              <div className="panel">
              <Table striped bordered condensed hover>
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Pais</th>
                    <th>Puntos</th>
                    <th>Diff</th>
                    <th>Gf</th>
                    <th>Gc</th>
                  </tr>
                </thead>
                <tbody>
                {elements}
                </tbody>
              </Table>
              </div>
            </Col>
          </Row>
          
      );
  }
}

// swap function helper
function swap(array, i, j) {
  var temp = array[i];
  array[i] = array[j];
  array[j] = temp;
}

function countrySort(object){
  let keys = Object.keys(object)
  for (var i = 0; i < keys.length; i++) {
    for (var j = 1; j < keys.length; j++) {
      if(object[keys[j - 1]].points > object[keys[j]].points) {
        swap(keys, j - 1, j);
      }
      else if (object[keys[j - 1]].points === object[keys[j]].points){
        let diff1 = object[keys[j - 1]].gf - object[keys[j - 1]].gc
        let diff2 = object[keys[j]].gf - object[keys[j]].gc
        if (diff1 > diff2) swap(keys, j - 1, j);
        else if (diff1 === diff2){
          if (object[keys[j - 1]].gf > object[keys[j]].gf) swap(keys, j - 1, j);
        }
      }
    }
  }
  return keys
}



