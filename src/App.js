import PropTypes from 'prop-types';
import React from 'react';
import HomePage from './home';

import { Responsive } from 'semantic-ui-react';

const API = 'https://twocaps-api.herokuapp.com';

/* Heads up!
 * Neither Semantic UI, nor Semantic UI React don't offer a responsive navbar, hover it can be easily implemented.
 * It can be more complicated, but you can create really flexible markup.
 */
const DesktopContainer = ({ children }) => (
  <Responsive {...Responsive.onlyComputer}>{children}</Responsive>
);

DesktopContainer.propTypes = {
  children: PropTypes.node,
};

const MobileContainer = ({ children }) => (
  <Responsive {...Responsive.onlyMobile}>{children}</Responsive>
);

MobileContainer.propTypes = {
  children: PropTypes.node,
};

const ResponsiveContainer = ({ children, ...props }) => (
  <div>
    <DesktopContainer>{children}</DesktopContainer>
    <MobileContainer>{children}</MobileContainer>
  </div>
);

ResponsiveContainer.propTypes = {
  children: PropTypes.node,
};

const HomepageLayout = () => (
  <ResponsiveContainer>
    <HomePage API={API} />
  </ResponsiveContainer>
);

export default HomepageLayout;
