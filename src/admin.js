import React, { Component } from 'react';
import axios from 'axios';
import {
  Divider,
  Segment,
  Grid,
  Button,
  List,
  Input,
  Modal,
  Message,
  Dimmer,
  Loader,
} from 'semantic-ui-react';

class Admin extends Component {
  state = {
    fetch: false,
    req: null,
    token: null,
    tweets: [],
    subjects: [],
    emails: [],
    logs: [],
    tweet: '',
    subject: '',
    email: '',
    username: '',
    password: '',
    loginError: '',
  };

  componentDidUpdate() {
    const { req, fetch } = this.state;
    if (req && !fetch) {
      req
        .get('/emails')
        .then(({ data: { emails } }) => this.setState({ emails }))
        .catch(error => console.log(error));
      req
        .get('/subjects')
        .then(({ data: { subjects } }) => this.setState({ subjects }))
        .catch(error => console.log(error));
      req
        .get('/tweets')
        .then(({ data: { tweets } }) => this.setState({ tweets }))
        .catch(error => console.log(error));
      req
        .get('/logs')
        .then(({ data: { logs } }) => this.setState({ logs }))
        .catch(error => console.log(error));
      this.setState({ fetch: true });
    }
  }

  handleChange = e => this.setState({ [e.target.name]: e.target.value });

  handleSubmit = (value, table) => {
    const { req, username } = this.state;
    req
      .post(`/add-${table}`, {
        [table]: value,
        added_by: username,
      })
      .then(({ data }) =>
        this.setState(prevState => ({
          [table]: '',
          [`${table}s`]: prevState[`${table}s`].concat(data[table]),
        }))
      )
      .catch(error => console.log(error.response));
  };

  handleRemove = (table, id) => {
    const { req } = this.state;
    req
      .delete(`/remove-${table}`, {
        data: {
          id,
        },
      })
      .then(({ data: { id } }) =>
        this.setState(prevState => ({
          [table]: '',
          [`${table}s`]: prevState[`${table}s`].filter(item => item.id !== id),
        }))
      )
      .catch(error => console.log(error.response));
  };

  handleLogin = () => {
    const { username, password } = this.state;
    if (username && password) {
      axios
        .post(`${this.props.API}/public/login`, {
          email: username,
          password: password,
        })
        .then(({ data: { token } }) =>
          this.setState({
            token,
            req: axios.create({
              baseURL: `${this.props.API}/private`,
              headers: {
                Authorization: `Bearer ${token}`,
              },
            }),
          })
        )
        .catch(({ response }) => this.setState({ loginError: response }));
    }
  };

  render() {
    const {
      token,
      loginError,
      username,
      password,
      tweet,
      subject,
      email,
      tweets,
      subjects,
      emails,
      logs,
    } = this.state;

    return (
      <Segment style={{ padding: '8em 0em' }} vertical>
        <Modal size="mini" dimmer="blurring" open={!token}>
          <Modal.Content>
            <Input
              name="username"
              value={username}
              onChange={this.handleChange}
              fluid
              icon="user"
              iconPosition="left"
              placeholder="Email"
            />
            <br />
            <Input
              name="password"
              value={password}
              onChange={this.handleChange}
              fluid
              icon="privacy"
              iconPosition="left"
              placeholder="Password"
              type="password"
              onKeyDown={e => (e.keyCode === 13 ? this.handleLogin() : null)}
            />
            {loginError && (
              <Message negative>
                <Message.Header>Login Failed.</Message.Header>
              </Message>
            )}
          </Modal.Content>
          <Modal.Actions>
            <Button
              positive
              icon="sign in"
              content="Sign In"
              onClick={this.handleLogin}
            />
          </Modal.Actions>
        </Modal>
        <Grid container stackable verticalAlign="top">
          <Grid.Row columns={3}>
            <Grid.Column>
              <Input
                name="tweet"
                value={tweet}
                onKeyDown={e =>
                  e.keyCode === 13 ? this.handleSubmit(tweet, 'tweet') : null
                }
                onChange={this.handleChange}
                fluid
                icon="twitter"
                iconPosition="left"
                label={{
                  as: 'a',
                  color: 'blue',
                  tag: true,
                  onClick: e => this.handleSubmit(tweet, 'tweet'),
                  content: 'Add Tweet',
                }}
                labelPosition="right"
                placeholder="Tweet"
              />
              <List animated divided verticalAlign="middle">
                {tweets ? (
                  tweets.map(val => (
                    <List.Item key={val.id}>
                      <List.Content floated="right">
                        <Button
                          circular
                          onClick={() => this.handleRemove('tweet', val.id)}
                          icon="delete"
                          color="red"
                        />
                      </List.Content>
                      <List.Content>
                        <List.Header>{val.tweet}</List.Header>
                        <List.Description>{val.added_by}</List.Description>
                      </List.Content>
                    </List.Item>
                  ))
                ) : (
                  <Dimmer inverted active>
                    <Loader inverted content="Loading" />
                  </Dimmer>
                )}
              </List>
            </Grid.Column>
            <Grid.Column>
              <Input
                name="subject"
                value={subject}
                onKeyDown={e =>
                  e.keyCode === 13
                    ? this.handleSubmit(subject, 'subject')
                    : null
                }
                onChange={this.handleChange}
                fluid
                icon="inbox"
                iconPosition="left"
                label={{
                  as: 'a',
                  color: 'blue',
                  tag: true,
                  onClick: () => this.handleSubmit(subject, 'subject'),
                  content: 'Add Subject',
                }}
                labelPosition="right"
                placeholder="Subject line"
              />
              <List animated divided verticalAlign="middle">
                {subjects ? (
                  subjects.map(val => (
                    <List.Item key={val.id}>
                      <List.Content floated="right">
                        <Button
                          circular
                          onClick={() => this.handleRemove('subject', val.id)}
                          icon="delete"
                          color="red"
                        />
                      </List.Content>
                      <List.Content>
                        <List.Header>{val.subject}</List.Header>
                        <List.Description>{val.added_by}</List.Description>
                      </List.Content>
                    </List.Item>
                  ))
                ) : (
                  <Dimmer inverted active>
                    <Loader inverted content="Loading" />
                  </Dimmer>
                )}
              </List>
            </Grid.Column>
            <Grid.Column>
              <Input
                name="email"
                value={email}
                onKeyDown={e =>
                  e.keyCode === 13 ? this.handleSubmit(email, 'email') : null
                }
                onChange={this.handleChange}
                fluid
                icon="mail"
                iconPosition="left"
                label={{
                  as: 'a',
                  color: 'blue',
                  tag: true,
                  onClick: () => this.handleSubmit(email, 'email'),
                  content: 'Add Email',
                }}
                labelPosition="right"
                placeholder="Email"
              />
              <List animated divided verticalAlign="middle">
                {emails ? (
                  emails.map(val => (
                    <List.Item key={val.id}>
                      <List.Content floated="right">
                        <Button
                          circular
                          onClick={() => this.handleRemove('email', val.id)}
                          icon="delete"
                          color="red"
                        />
                      </List.Content>
                      <List.Content>
                        <List.Header>{val.email}</List.Header>
                        <List.Description>{val.added_by}</List.Description>
                      </List.Content>
                    </List.Item>
                  ))
                ) : (
                  <Dimmer inverted active>
                    <Loader inverted content="Loading" />
                  </Dimmer>
                )}
              </List>
            </Grid.Column>
          </Grid.Row>
          <Divider
            horizontal
            style={{ margin: '3em 0em', textTransform: 'uppercase' }}
          />
          <Grid.Row>
            <Grid.Column>
              <h2>Logs</h2>
              <List animated divided verticalAlign="middle">
                {logs ? (
                  logs.map(val => (
                    <List.Item key={val.id}>
                      <List.Content floated="right">
                        <Button
                          circular
                          onClick={() => this.handleRemove('log', val.id)}
                          icon="delete"
                          color="red"
                        />
                      </List.Content>
                      <List.Content>
                        <List.Header>
                          {val.createdAt}: {val.subject} | {val.email}
                        </List.Header>
                        <List.Description>
                          {val.firstname} {val.lastname} | {val.useremail} |{' '}
                          {val.userpostal}
                        </List.Description>
                      </List.Content>
                    </List.Item>
                  ))
                ) : (
                  <Dimmer inverted active>
                    <Loader inverted content="Loading" />
                  </Dimmer>
                )}
              </List>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
    );
  }
}

export default Admin;
