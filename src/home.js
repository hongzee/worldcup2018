import React, { Fragment } from 'react';
import axios from 'axios';
import has from 'lodash/has';

import {
  Button,
  Container,
  Dimmer,
  Divider,
  Form,
  Grid,
  Header,
  Icon,
  Input,
  Loader,
  Segment,
  Message,
  Image,
} from 'semantic-ui-react';

import Logo from './assets/logo.svg';

export default class Home extends React.PureComponent {
  state = {
    tweet: {},
    email: {},
    subject: {},
    emailError: false,
    sent: false,
    firstname: '',
    lastname: '',
    userpostal: '',
    useremail: '',
  };

  componentDidMount() {
    axios
      .get(`${this.props.API}/public/tweet`)
      .then(({ data }) => this.setState({ tweet: data.tweet }))
      .catch(error => console.log(error));
    axios
      .get(`${this.props.API}/public/email`)
      .then(({ data }) => this.setState({ email: data.email }))
      .catch(error => console.log(error));
    axios
      .get(`${this.props.API}/public/subject`)
      .then(({ data }) => this.setState({ subject: data.subject }))
      .catch(error => console.log(error));
  }

  handleChange = e =>
    this.setState({ emailError: false, [e.target.name]: e.target.value });

  handleEmail = () => {
    const {
      subject,
      email,
      firstname,
      lastname,
      useremail,
      userpostal,
    } = this.state;
    if (firstname && lastname && useremail) {
      axios
        .post(`${this.props.API}/public/log`, {
          subject: subject.id,
          email: email.id,
          firstname,
          lastname,
          useremail,
          userpostal,
        })
        .then(response => this.setState({ sent: true, response }))
        .catch(error => this.setState({ emailError: true, error }));
    } else {
      this.setState({ emailError: true });
    }
  };

  handlePostal = e => {
    e.persist();
    this.setState(
      ({ userpostal }) =>
        userpostal.length === 2 && e.target.value.length > 2
          ? { userpostal: e.target.value.toUpperCase() + ' ' }
          : { userpostal: e.target.value.toUpperCase() }
    );
  };

  render() {
    const {
      tweet,
      email,
      subject,
      firstname,
      lastname,
      useremail,
      userpostal,
    } = this.state;

    return (
      <Fragment>
        <Segment vertical style={{ padding: '5em 0em' }}>
          <Container>
            <Grid columns={2} centered inverted stackable>
              <Grid.Column>
                <Image centered src={Logo} size="large" />
              </Grid.Column>
            </Grid>
          </Container>
        </Segment>
        <Segment
          vertical
          inverted
          style={{ padding: '8em 0em', backgroundColor: '#ae2225' }}
        >
          <Container>
            <Grid columns={2} centered inverted stackable>
              <Grid.Column textAlign="right">
                <Message icon size="huge" floating>
                  {has(tweet, 'tweet') ? (
                    <Icon name="twitter" />
                  ) : (
                    <Icon name="circle notched" loading />
                  )}
                  <Message.Header style={{ marginLeft: 'auto' }}>
                    {has(tweet, 'tweet') ? tweet.tweet : 'loading tweet...'}
                  </Message.Header>
                </Message>
                <Button
                  as="a"
                  size="huge"
                  color="black"
                  target="_blank"
                  href={`https://twitter.com/intent/tweet?text=${tweet}`}
                >
                  Tweet
                </Button>
              </Grid.Column>
            </Grid>
          </Container>
        </Segment>
        <Segment style={{ padding: '8em 0em' }} vertical>
          <Container text>
            <Message
              floating
              color="black"
              header={
                <div
                  style={{
                    fontSize: '1.2em',
                    fontWeight: 800,
                    textAlign: 'center',
                  }}
                >
                  Let Minister Morneau know you want to keep your points
                </div>
              }
              size="massive"
            />
            <Divider
              as="h4"
              horizontal
              style={{ margin: '3em 0em', textTransform: 'uppercase' }}
            />
            <h3>Subject</h3>
            <p>
              {has(subject, 'subject') ? (
                subject.subject
              ) : (
                <Dimmer inverted active>
                  <Loader inverted content="Loading" />
                </Dimmer>
              )}
            </p>
            <Form>
              <Divider horizontal />
              <Header as="h3" style={{ fontSize: '2em' }}>
                Minister Morneau,
              </Header>
              <p style={{ fontSize: '1.33em' }}>
                {has(email, 'email') ? email.email : <Loader />}
              </p>
              <Divider horizontal />
              <p style={{ fontSize: '1.33em' }}>Sincerely,</p>
              <Divider horizontal />
              <Form.Group widths="equal">
                <Form.Field
                  name="firstname"
                  value={firstname}
                  onChange={this.handleChange}
                  control={Input}
                  label="First name"
                  placeholder="First name"
                />
                <Form.Field
                  name="lastname"
                  value={lastname}
                  onChange={this.handleChange}
                  control={Input}
                  label="Last name"
                  placeholder="Last name"
                />
              </Form.Group>
              <Form.Group widths="equal">
                <Form.Field
                  name="useremail"
                  value={useremail}
                  onChange={this.handleChange}
                  type="email"
                  control={Input}
                  label="Email"
                  placeholder="example@domain.com"
                />
                <Form.Field
                  name="userpostal"
                  value={userpostal}
                  label="Postal Code"
                  control={Input}
                  onChange={this.handlePostal}
                  type="text"
                  pattern="[A-Za-z][0-9][A-Za-z] [0-9][A-Za-z][0-9]"
                  title="Format must be A0A 0A0"
                  placeholder="A0A 0A0"
                  maxLength="7"
                />
              </Form.Group>
              <Divider horizontal />
              <Button as="a" size="large" onClick={this.handleEmail}>
                <Icon name="send" /> Send
              </Button>
              <Divider horizontal />
              {this.state.sent && (
                <Message positive>
                  <Message.Header>Your email has been sent!</Message.Header>
                </Message>
              )}
              {this.state.emailError && (
                <Message negative>
                  <Message.Header>
                    All mandatory fields must be completed.
                  </Message.Header>
                </Message>
              )}
            </Form>
          </Container>
        </Segment>
        <Segment
          vertical
          style={{ padding: '3em 0em', backgroundColor: '#AE2225' }}
        >
          <Container>
            <Grid columns={2} centered inverted stackable>
              <Grid.Column>
                <Image centered src={Logo} size="small" />
              </Grid.Column>
            </Grid>
          </Container>
        </Segment>
      </Fragment>
    );
  }
}
